# CouchDB - Options with Docker, Cluster, etc

This is almost a learning project. I have built a CouchDB image from Debian:stable. I **strongly** recommend to use CouchDB official docker image.

There is also a Bash script that configures a cluster. It use an array as server array names (IPs or FQDN), and it needs to know admin user.