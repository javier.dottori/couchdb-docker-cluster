# adapted from: http://docs.couchdb.org/en/stable/setup/cluster.html
# by jdottori
# in file vm.args
# ...
export ADMIN="admin"
export PASSWORD="j"
# to avoid password you can use --user $ADMIN on each CURL
# and change the SERVERS array creation as indicated
export PORT=5984
export IPS=("c1" "c2" "c3")
export PROTOCOL="http"
#export IPS=("172.17.0.2" "172.17.0.3" "172.17.0.4")

export SERVERS=()
export COUNT=${#IPS[@]}

# first step set admin password, 
# ensuring admin has password, the script after this is easier
for IP  in ${IPS[@]}; do
    export NA_SERVER="$PROTOCOL://${IP}:$PORT"
    echo $NA_SERVER 
    echo "1. Create the admin user and password:"
    #NOTE: actual user is set in the server URL! be carefull if U change it
    #curl -X PUT $NA_SERVER/_node/_local/_config/admins/$ADMIN -d '"'$PASSWORD'"'
    SERVERS+=("$PROTOCOL://$ADMIN:$PASSWORD@${IP}:$PORT")
    #swap those lines to manually input password  
    #SERVERS+=("$PROTOCOL://${IP}:$PORT")
done
export ORGANIZER=${SERVERS[0]}
export CLUSTER_UUID=$(curl -s $ORGANIZER/_uuids | cut -d'"' -f 4)
export SHARED_SECRET=$(curl -s $ORGANIZER/_uuids | cut -d'"' -f 4)

# CouchDB will respond with something like:
#   {"uuids":["60c9e8234dfba3e2fdab04bf92001142","60c9e8234dfba3e2fdab04bf92001cc2"]}
# Copy the provided UUIDs into your clipboard or a text editor for later use.
# Use the first UUID as the cluster UUID.
# Use the second UUID as the cluster shared http secret.

for s  in ${SERVERS[@]}; do 
    echo "Configuring $s" 
    echo "1. Create the admin user and password:"
    #NOTE: actual user is set in the server URL! be carefull if U change it
    #curl -X PUT $s/_node/_local/_config/admins/$ADMIN -d '"'$PASSWORD'"'
    echo "2. Bind the clustered interface to all IP addresses availble:"
    curl -X PUT $s/_node/_local/_config/chttpd/bind_address -d '"0.0.0.0"'
    echo "3. Set the UUID of the node:"
    curl -X PUT $s/_node/_local/_config/couchdb/uuid -d '"'$CLUSTER_UUID'"'
    echo "4. Set the shared http secret for cookie creation:"
    curl -X PUT $s/_node/_local/_config/couch_httpd_auth/secret -d '"'$SHARED_SECRET'"'
done

for SERVER  in ${SERVERS[@]}; do 
    curl -X GET $SERVER/_cluster_setup
    # On EACH NODE start cluster setup
    curl -X POST -H "Content-Type: application/json" $SERVER/_cluster_setup -d '{"action": "enable_cluster", "bind_address":"0.0.0.0", "username": "'$ADMIN'", "password":"'$PASSWORD'", "node_count":"'$COUNT'"}'
done
# Now use ORGANIZER as the setup coordination node 
# (there is no master after seting up the cluster)
for serverIp  in ${IPS[@]:1}; do 
    echo "Joining $serverIp" 
    curl -X POST -H "Content-Type: application/json" $ORGANIZER/_cluster_setup -d '{"action": "enable_cluster", "bind_address":"0.0.0.0", "username": "'$ADMIN'", "password":"'$PASSWORD'", "port": '$PORT', "node_count": "'$COUNT'", "remote_node": "'$serverIp'", "remote_current_user": "'$ADMIN'", "remote_current_password": "'$PASSWORD'" }'
done
for serverIp  in ${IPS[@]:1}; do 
    echo "Joining $serverIp" 
    curl -X POST -H "Content-Type: application/json" $ORGANIZER/_cluster_setup -d '{"action": "add_node", "host":"'$serverIp'", "port":'$PORT', "username": "'$ADMIN'", "password":"'$PASSWORD'"}'
done

curl -X POST -H "Content-Type: application/json" $ORGANIZER/_cluster_setup -d '{"action": "finish_cluster"}'

curl $ORGANIZER/_cluster_setup
#Expected: {"state":"cluster_finished"} (according to docs)
#but receiving "single_node_enabled" and it works fine (2019-02)

curl -X GET $ORGANIZER/_membership
curl -X GET $ORGANIZER/_cluster_setup 