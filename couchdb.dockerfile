FROM debian:stretch
# install tooling
RUN apt-get update && \
    apt-get -y upgrade
RUN apt-get install -y sudo gnupg2 curl apt-transport-https

# install couchdb
RUN echo "deb https://apache.bintray.com/couchdb-deb stretch main" | sudo tee -a /etc/apt/sources.list
RUN curl -L https://couchdb.apache.org/repo/bintray-pubkey.asc | sudo apt-key add -
RUN apt-get update
# set devconf-set-selections to do an unattended install
# unattended install script from:
# https://github.com/apache/couchdb-pkg/blob/master/debian/README.Debian
ENV COUCHDB_PASSWORD=j
RUN echo "couchdb couchdb/mode select standalone \
        couchdb couchdb/mode seen true \
        couchdb couchdb/bindaddress string 0.0.0.0 \
        couchdb couchdb/bindaddress seen true \
        couchdb couchdb/adminpass password ${COUCHDB_PASSWORD} \
        couchdb couchdb/adminpass seen true \
        couchdb couchdb/adminpass_again password ${COUCHDB_PASSWORD} \
        couchdb couchdb/adminpass_again seen true" | debconf-set-selections
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y --force-yes couchdb

RUN apt-get install -y
# Add Tini
ENV TINI_VERSION v0.18.0
ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini /tini
RUN chmod +x /tini
ENTRYPOINT ["/tini", "--"]

# bind address = 0.0.0.0 to allow access from any network interface
RUN sed -i.bak "s|bind_address = 127.0.0.1|bind_address = 0.0.0.0|g" /opt/couchdb/etc/default.ini
RUN sudo apt-get install -y nano

#EXPOSE 5984 4369 9100-9200
#VOLUME ["/opt/couchdb/data"]

# Run your program under Tini
CMD ["/opt/couchdb/bin/couchdb"]
# or docker run your-image /your/program ...
