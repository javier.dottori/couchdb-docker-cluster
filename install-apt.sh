# from: https://www.hugeserver.com/kb/how-install-apache-couchdb-ubuntu-16/
# install tooling
apt update
apt-get install sudo gnupg2 curl apt-transport-https

# install couchdb
echo "deb https://apache.bintray.com/couchdb-deb stretch main" | sudo tee -a /etc/apt/sources.list
curl -L https://couchdb.apache.org/repo/bintray-pubkey.asc | sudo apt-key add -
apt update
# DOCKER: after apt-get install; enable service to start 
# even I am in a container (dark magic)
# don t do this in docker build files 
# echo "exit 0" > /usr/sbin/policy-rc.d
apt install couchdb