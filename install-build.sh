# from: http://docs.couchdb.org/en/stable/install/unix.html
# and
# from: https://www.hackster.io/mehealth-ch/installing-couchdb-on-raspbian-stretch-ccb2a7
apt-get update && \
    apt-get -y upgrade
#apt-get install -y sudo gnupg2 curl apt-transport-https

sudo apt-get --no-install-recommends -y install \
    build-essential pkg-config erlang \
    libicu-dev libmozjs185-dev libcurl4-openssl-dev \
	wget sudo curl
#download uncompress and build
cd
VERSION="2.3.0"
wget http://mirror.ibcp.fr/pub/apache/couchdb/source/$VERSION/apache-couchdb-$VERSION.tar.gz   
tar zxvf apache-couchdb-$VERSION.tar.gz
cd apache-couchdb-$VERSION/
./configure
make release

#configure users and dbms
adduser --system \
        --shell /bin/bash \
        --group --gecos \
        "CouchDB Administrator" couchdb
cp -Rp rel/couchdb/* /home/couchdb
chown -R couchdb:couchdb /home/couchdb
#remove others permissions on directories
find /home/couchdb -type d -exec chmod 0770 {} \;
chmod 0644 /home/couchdb/etc/*

sudo nano /home/couchdb/etc/local.ini
# => change line
#   bind_address = 127.0.0.1
# to:
#	bind_address = 0.0.0.0
# START CouchDB
sudo -i -u couchdb /home/couchdb/bin/couchdb